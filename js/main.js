window.addEventListener('DOMContentLoaded', () => {
   const tabsContent = document.querySelectorAll('.tabs-content li');
   const tabsParent = document.querySelector('.tabs');

   function hideAllAndShowFirstTabContent() {
      tabsContent.forEach(item => {
         item.style.display = 'none';
      });
      tabsContent[0].style.display = 'block';
   }

   hideAllAndShowFirstTabContent();

   tabsParent.addEventListener('click', (event) => {
      const target = event.target;
      if (target.classList.contains('tabs-title')) {
         const tabAttr = target.getAttribute('data-name');

         tabsContent.forEach(content => {
            content.style.display = content.id === tabAttr ? 'block' : 'none';
         });

         const activeTab = tabsParent.querySelector('.tabs-title.active');
         if (activeTab) {
            activeTab.classList.remove('active');
         } 
         target.classList.add('active');
      }
   });

});














